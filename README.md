# Blink

Project I am writing while "relearning" PIC development.

The code, once compiled and sent to a compatible PIC device,
will blink a LED which is connected to the PORTB 0 pin, i.e.
pin 17 of a dsPIC30F4013 which was the device used in development.

The project was designed with the "dsPIC30F4013 Breadboard PIC"
in mind. More details [here](https://gitlab.com/BreadboardPIC/dspic30f4013)
