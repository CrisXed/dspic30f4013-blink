/*
 * File:   main.c
 * Author: cristovao
 *
 * Created on June 29, 2022, 12:58 PM
 */

// FOSC
#pragma config FOSFPR = XT              // Oscillator (XT)
#pragma config FCKSMEN = CSW_FSCM_ON    // Clock Switching and Monitor (Sw Enabled, Mon Enabled)

// FWDT
#pragma config FWPSB = WDTPSB_16        // WDT Prescaler B (1:16)
#pragma config FWPSA = WDTPSA_512       // WDT Prescaler A (1:512)
#pragma config WDT = WDT_OFF            // Watchdog Timer (Disabled)

// FBORPOR
#pragma config FPWRT = PWRT_4           // POR Timer Value (4ms)
#pragma config BODENV = BORV20          // Brown Out Voltage (Reserved)
#pragma config BOREN = PBOR_ON          // PBOR Enable (Enabled)
#pragma config MCLRE = MCLR_EN          // Master Clear Enable (Enabled)

// FGS
#pragma config GWRP = GWRP_OFF          // General Code Segment Write Protect (Disabled)
#pragma config GCP = CODE_PROT_OFF      // General Segment Code Protection (Disabled)

// FICD
#pragma config ICS = ICS_PGD            // Comm Channel Select (Use PGC/EMUC and PGD/EMUD)

// Clock frequency in Hz (Frequency of the OSCillator - FOSC)
#define XTFREQ 7372800. // xtal = 7.3728 MHz
#define PLLMODE 1.
#define POSTSCALER 1.
#define FOSC (XTFREQ*PLLMODE/POSTSCALER)
#define FCY (FOSC/4.)

// TOSC - Time of the OSCillator

#include "xc.h"
#include <stdbool.h>
#include <stdio.h>


#define LEDPIN  LATBbits.LATB0
#define LEDTRIS TRISBbits.TRISB0

bool onFallbackClk = false;

// Add a Clock Failure Trap to handle issues with the clock
// If it does happen, the FRC oscillator will be used in the
// trap and we can swap back to the correct one if desired
void __interrupt(auto_psv) _OscillatorFail(void)
{
    if(OSCCONbits.CF)
    {
        //OSCCONbits.CF = 0; // Reset the failure flag
        // Does not work, because the write sequence has not been done

        if(onFallbackClk)
        {
            // Add an error
        }
        else
        {
            // It seems all the code below may be unnecessary because the FSCM
            // already swaps to the FRC in case of a failure

            //FRC is 7.37 MHz which is close enough to the external crystal (7.3728 MHz)
            //so use it as a fallback

            // Disable interrupts
            //__builtin_disi(0x3FFF);
            __builtin_disable_interrupts();

            uint8_t newClk = 0b001; // bits for defining the FRC
            __builtin_write_OSCCONH(newClk);
            /*OSCCONH = 0x78;
            OSCCONH = 0x9A;
            OSCCONH = newClk;*/
            
            uint8_t newLowByte = OSCCONL | 0b00000001; // Turn on clock change bit
            newLowByte = newLowByte & 0b11110111; // Disable the clock failure flag
            __builtin_write_OSCCONL(newLowByte);
            /*OSCCONL = 0x46;
            OSCCONL = 0x57;
            OSCCONL = newLowByte;*/

            while(OSCCONbits.OSWEN == 1); // TODO: Maybe change logic for determining end

            if(OSCCONbits.COSC == newClk)
                onFallbackClk = true;
            
            //__builtin_disi(0x0);
            __builtin_enable_interrupts();
        }
    }
    
    INTCON1bits.OSCFAIL = 0;
}

// Interrupt for timer 1, which will be used for Toggling an LED
void __interrupt(auto_psv) _T1Interrupt(void)
{
    LEDPIN = ~LEDPIN; // Toggle the LED state
    
    if(LEDPIN)
    {
        //printf("Turn LED on\n");
    }
    else
    {
        //printf("Turn LED off\n");
    }

    IFS0bits.T1IF = 0;
}

int main(void)
{
    RCONBITS oldRCON = RCONbits;
    RCON = RCON & 0b0011111100100000; // Reset Flags from RCON

    if( oldRCON.POR && oldRCON.BOR) // If Power On Reset
    {}

    if(!oldRCON.POR && oldRCON.BOR) // If Brown Out Reset
    {}

    if( oldRCON.EXTR) // If MCLR
    {
        if(!oldRCON.IDLE && !oldRCON.SLEEP) // If Regular MCLR
        {}

        if(!oldRCON.IDLE &&  oldRCON.SLEEP) // If MCLR during Sleep
        {}

        if( oldRCON.IDLE && !oldRCON.SLEEP) // If MCLR during Idle
        {}
    }

    if(oldRCON.SWR) // If Software Reset
    {}

    if(oldRCON.WDTO) // If WDT Time-Out Reset
    {}

    if(oldRCON.TRAPR) // If Trap Reset
    {}

    if(oldRCON.IOPUWR) // If Illegal Operation Trap
    {}
    
    // Disable peripherals by default for minimum power consumption
    PMD1 =
       0b1000000000000000 | //T5MD
       0b0100000000000000 | //T4MD
       0b0010000000000000 | //T3MD
       0b0001000000000000 | //T2MD
       //0b0000100000000000 | //T1MD
       0b0000000100000000 | //DCIMD
       0b0000000010000000 | //I2CMD
       0b0000000001000000 | //U2MD
       0b0000000000100000 | //U1MD
       0b0000000000001000 | //SPI1MD
       0b0000000000000010 | //C1MD
       0b0000000000000001;  //ADCMD
    
    PMD2 =
       0b1000000000000000 | //IC8MD
       0b0100000000000000 | //IC7MD
       0b0000001000000000 | //IC2MD
       0b0000000100000000 | //IC1MD
       0b0000000000001000 | //OC4MD
       0b0000000000000100 | //OC3MD
       0b0000000000000010 | //OC2MD
       0b0000000000000001;  //OC1MD
    
    // Set the output pin for the LED
    LEDTRIS = 0;
    LEDPIN  = 0;
    
    //printf("Done Configuring\n");
    
    TMR1 = 0; // Reset Timer1 counter
    PR1 = 1/(256./FCY);
    //printf("PR1 = %i\n", PR1);

    IFS0bits.T1IF = 0;     // Reset the timer interrupt flag
    IPC0bits.T1IP = 0b100; // Set the interrupt priority
    IEC0bits.T1IE = 1;     // Enable the timer interrupt

    T1CONbits.TCS = 0;      // Use internal clock
    T1CONbits.TCKPS = 0b11; // 1:256 prescale
    T1CONbits.TON = 1;      // Start Timer1

    while(1)
    {}

    return 0;
}
